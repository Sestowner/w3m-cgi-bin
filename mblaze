#!/bin/sh

LOCATION="${HOME}/.local/mail"
MAILRC="${HOME}/.config/mail/mailrc"

. "${MAILRC}"
active_account="$(basename $(realpath "${HOME}/.mblaze"))"

scan()
{
	mscan -f 'href="?show=%n">%c%u%r&nbsp;%-3n&nbsp;%10d&nbsp;%20f&nbsp;%t&nbsp;%2i%s' $@ |\
		awk '{gsub(" ","\\&nbsp;", $0);printf("<a %s</a><br>", $0)}'
	local l="?ls=${active_account}"
	printf '<br><a href="%s&sync">SYNC</a>' "$l"
	printf '<br><a href="%s">← BACK</a>' "$l"
}

list_accounts()
{
	grep -E '^\w+\(\)$' $MAILRC | while read l; do
		local acc="${l%%(*}"
		$(echo "$acc")
		if [ -d "${LOCATION}/${acc}" ]
		then
			echo "${l%%(*}" "$(mdirs -a "${LOCATION}/${acc}" | mlist -iN | tail -1 | awk '{print $1}')" $([ "$active_account" = "$acc" ] && echo "<b>${LOGIN}</b>" || echo "$LOGIN")
		fi
	done | sort -t' ' -k2 -r | xargs printf '<a href="?ls=%s">%s	%s</a><br>\n'
	printf '<br><a href="?%s&sync">SYNC</a>' "$QUERY_STRING"
}

change_account()
{
	$(basename "$1") || exit
	[ ! "$MBLAZE" ] && exit
	[ "$MBLAZE" = "$(realpath "${HOME}/.mblaze")" ] && return
	unlink "${HOME}/.mblaze" || exit
	ln -s "$MBLAZE" "${HOME}/.mblaze" || exit
}

list_dirs()
{
	local wd="${LOCATION}/${1}"
	if [ $(expr "$1" : '\w*/\w*') -gt 0 ]
	then
		$([ "$(mlist -iN "$wd" | awk '{print $1}')" -eq 0 ] && echo mlist || echo minc) "$wd" | mthread -r | mseq -S > /dev/null
		scan :
	else
		change_account "$wd"
		mdirs -a "$wd" | mlist -iN | sort -r | awk -vL="${wd}/" -vP="$1" '{if($7){gsub(L, "", $7);printf("<a href=\"?ls=%s/%s\">%-5.5s %s</a><br>\n", P, $7, $1, $7)}}'
		printf '<br><a href="?%s&sync">SYNC</a>' "$QUERY_STRING"
		echo '<br><a href="?acc">← BACK</a>'
	fi
}

show_message()
{
	mseq -C "$1"

	local total=$(mscan -n -- -1)

	case "$1" in
		1) scan .-0:.+5 ;;
		2) scan .-1:.+4 ;;
		$((total - 2))) scan .-3:.+2 ;;
		$((total - 1))) scan .-4:.+1 ;;
		$total) scan .-5:.+0 ;;
		*) scan .-2:.+3 ;;
	esac

	echo '<hr>'

	if mshow -t "$1" | grep -qF 'text/plain'
	then
		mshow "$1" | sed '/^$/{N;/^\n$/d;}' | sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g' | sed -e ':a;N;$!ba;s/\n/&<br>/g' -e '$a<br>'
	else
		mshow "$1" | awk -vH=1 '
			{
				if (H)
				{
					if (!$0)
					{
						print "<hr>";
						H=0;
					}
					else
					{
						gsub("<", "\\&lt;", $0);
						gsub(">", "\\&gt;", $0);
						printf("%s<br>", $0);
					}
				}
				else if ($1 == "---" && $NF == "---") printf("%s<br>", $0);
				else print;
			}
			'
	fi
}

get_qv()
{
	echo "$QUERY_STRING" | tr '&' '\n' | while read -r item
	do
		local k="${item%%=*}"
		local v="${item##*=}"
		if [ "$k" = "$1" ]
		then
			echo "$v"
			return
		fi
	done
}


QUERY_STRING="${QUERY_STRING:-scan}"

printf 'Content-type: text/html\n'
if [ $(get_qv sync) ]
then
	channel="$(get_qv ls | sed 's;/;:;')"
	printf "w3m-control: EXEC_SHELL mbsync %s\n" "${channel:--a}"
	printf "w3m-control: GOTO %s\n" "${W3M_URL%%\&sync}"
	printf 'w3m-control: DELETE_PREVBUF\n'
	printf 'w3m-control: DELETE_PREVBUF\n'
	exit
fi
printf '\n'

case "$QUERY_STRING" in
	scan) scan : ;;
	acc) list_accounts ;;
	ls=[a-z]*) list_dirs $(get_qv ls) ;;
	show=[0-9]*) show_message $(get_qv show) ;;
esac

