#!/bin/sh

YT_URL="https://youtube.com"

request() {
	wget -T 20 -qO- $@
}

show_video() {
	request "${YT_URL}/${QUERY_STRING}" |\
	sed -En 's/.*var ytInitialPlayerResponse = (\{.*\});<\/script><div id="player.*/\1/p' |\
	jshon -e videoDetails -e title -u -pe author -u -pe channelId -u  -pe videoId -u -pe shortDescription -u |\
	awk '
		BEGIN {
			print "<table border=0>";
		}
		{
			if (NR > 5) {
				printf("%s<br>", $0);
				return
			}
			if(NR%5==1) {
				title=$0;
			} else if(NR%5==2) {
				author=$0;
			} else if(NR%5==3) {
				channelId=$0;
			} else if(NR%5==4) {
				videoId=$0;
				printf("<tr><td><a href=\"https://youtube.com/watch?v=%s\">%s</a></td></tr>\n", videoId, title);
				printf("<tr><td><a href=\"https://youtube.com/channel/%s\">%s</a></td></tr>\n", channelId, author);
			} else if(NR%5==0) {
				print "<tr><td>";
				printf("%s<br>", $0);
			}
		}
		END {
			print "</td></tr></table>";
		}
	'
}

show_videos() {
	request "${YT_URL}/${QUERY_STRING}" |\
	sed -En 's/.*var ytInitialData = (\{.*\});<\/script><script nonce=".*/\1/p' |\
	jshon -econtents  -etwoColumnBrowseResultsRenderer -etabs -e1 -etabRenderer -econtent -ae contents \
		-a -erichItemRenderer -econtent -evideoRenderer -evideoId -u -petitle -eaccessibility -eaccessibilityData -elabel -u |\
	awk -vP="${YT_URL}/watch?v=" '
		BEGIN {
			print "<table border=1>";
		}
		{
			if(NR%2==1) {
				id=$0;
			} else {
				printf("<tr><td><a href=%s%s>%s<a></td></tr>\n", P, id, $0);
			}
		}
		END {
			print "</table>";
		}
	'
}

show_playlists() {
	request "${YT_URL}/${QUERY_STRING}" |\
	sed -En 's/.*var ytInitialData = (\{.*\});<\/script><script nonce=".*/\1/p' |\
	jshon -QC -e contents -e twoColumnBrowseResultsRenderer -e tabs -a -e tabRenderer -e content \
		-e sectionListRenderer -e contents -a -e itemSectionRenderer -e contents -a -e gridRenderer \
		-e items -a -e gridPlaylistRenderer -e playlistId -u -pe title -e runs -e 0 -e text -u -pppp -e videoCountText -e runs -e 0 -e text -u |\
	sed '/^null$/d' |\
	awk -vP="${YT_URL}/playlist?list=" '
		BEGIN {
			print "<table border=1>";
		}
		{
			if(NR%3==1) {
				id=$0;
			} else if(NR%3==2) {
				title=$0;
			} else {
				printf("<tr><td><a href=%s%s>%s<a></td><td>%s</td></tr>\n", P, id, title, $0);
			}
		}
		END {
			print "</table>";
		}
	'
}

show_playlist() {
	request "${YT_URL}/${QUERY_STRING}" |\
	sed -En 's/.*var ytInitialData = (\{.*\});<\/script><script nonce=".*/\1/p' |\
	jshon -e contents -e twoColumnBrowseResultsRenderer -e tabs -a -e tabRenderer -e content \
		-e sectionListRenderer -e contents -a -e itemSectionRenderer -e contents -a -e playlistVideoListRenderer \
		-e contents -a -e playlistVideoRenderer -e videoId -u -pe title -e accessibility -e accessibilityData -e label -u |\
	awk -vP="${YT_URL}/watch?v=" '
		BEGIN {
			print "<table border=1>";
		}
		{
			if (NR%2==1)
				id=$0;
			else
				printf("<tr><td><a href=%s%s>%s<a></td></tr>\n", P, id, $0);
		}
		END {
			print "</table>";
		}
	'
}

show_channel() {
	request "${YT_URL}/${QUERY_STRING}" |\
	sed -En 's/.*var ytInitialData = (\{.*\});<\/script><script nonce=".*/\1/p' |\
	jshon -eheader -ae subscriberCountText -esimpleText -u -ppppemetadata -echannelMetadataRenderer -etitle -u -pe channelUrl -u -pe rssUrl -u -pedescription -u |\
	awk '
		{
			if (NR > 5) {
				printf("%s<br>\n", $0);
				return
			}
			if(NR%5==1) {
				subs=$0;
			} else if(NR%5==2) {
				title=$0;
			} else if(NR%5==3) {
				channelUrl=$0;
			} else if(NR%5==4) {
				printf("<link rel=\"alternate\" type=\"application/rss+xml\" title=\"%s\" href=\"%s\">\n", title, $0);
				printf("<table border=0><tr><td><a href=%s>%s<a></td><td>%s</td><td><a href=\"%s\">Subscribe</a></td></tr></table><hr>\n", channelUrl, title, subs, $0);
			} else if(NR%5==0) {
				printf("%s<br>", $0);
			}
		}
	'
	echo "<hr>"
	echo "<table border=1><tr>"
	echo "<td><a href=${YT_URL}/${QUERY_STRING}/videos>Videos</a></td>"
	echo "<td>Livestreams</td>"
	echo "<td><a href=${YT_URL}/${QUERY_STRING}/playlists>Playlists</a></td>"
	echo "<td>Community</td>"
	echo "<td>Channels</td>"
	echo "</tr></table>"
}

search() {
	request "${YT_URL}/results?$(expr "$QUERY_STRING" : '.*\(search_query=.*\)' | tr ' ' '+')" |\
	sed -En 's/.*var ytInitialData = (\{.*\});<\/script><script nonce=".*/\1/p' |\
	jshon -e contents -e twoColumnSearchResultsRenderer -e primaryContents -e sectionListRenderer \
		-e contents -a -e itemSectionRenderer -e contents |\
	awk -vP="${YT_URL}" '
		BEGIN {
			TYPE="";
			lvl=0;
			print "<table border=1>";
		}
		{
			if (TYPE != "" && lvl && $0 == substr("               ", 1, lvl)"}") {
				if (TYPE == "channel")
					printf("<tr><td><a href=%s/channel/%s>%s<a><br>%s</td></tr>\n", P, videoId, name, label);
				else if (TYPE == "video")
					printf("<tr><td><a href=%s/watch?v=%s>%s<a></td></tr>\n", P, videoId, label);
				lvl=0;
				TYPE = videoId = name = label = "";
			}
			if (TYPE == "video") {
				if ($1 == "\"videoId\":") {
					gsub(/[\",]/, "", $2);
					videoId = $2;
				} else if (!label && $1 == "\"label\":") {
					$1 = "";
					gsub(/[\",]/, "", $0);
					sub(" ", "", $0);
					label = $0;
				}
			} else if (TYPE == "channel") {
				if ($1 == "\"channelId\":") {
					gsub(/[\",]/, "", $2);
					videoId = $2;
				} else if (!label && $1 == "\"text\":") {
					$1 = "";
					gsub(/[\",]/, "", $0);
					sub(" ", "", $0);
					label = $0;
				} else if (!name && $1 == "\"simpleText\":") {
					$1 = "";
					gsub(/[\",]/, "", $0);
					sub(" ", "", $0);
					name = $0;
				}
			}
			if ($1 == "\"videoRenderer\":") {
				lvl=gsub(" ", "", $0)-1;
				TYPE="video";
			} else if ($1 == "\"channelRenderer\":") {
				lvl=gsub(" ", "", $0)-1;
				TYPE="channel";
			}
		}
		END {
			print "</table>";
		}
	'
}


printf 'Content-type: text/html\n'
printf '\n'
echo '<form><input name=search_query required></form>'
echo '<hr>'


case "$QUERY_STRING" in
	results?search_query=*|search_query=*) search ;;
	watch?v=*) show_video ;;
	playlist?list=*) show_playlist ;;
	channel/*/videos) show_videos ;;
	channel/*/playlists) show_playlists ;;
	channel/*) show_channel ;;
esac

